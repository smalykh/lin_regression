#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

void linear_regression(double* independent, double* dependent, int size, double *b0, double *b1);
double model_adequacy_check(double* independent, double* dependent, int size, double b0, double b1);
int generate_data(double* independent, double* dependent, double D, int n);

