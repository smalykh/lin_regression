#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "lin_regres.h"

static char DATASET_FILENAME[] = "dataset.txt";
static char LINE_FILENAME[] = "line.txt";

//--------------------------------------------------------
// FUNCTION printdata
//--------------------------------------------------------
void printdata(double* independent, double* dependent, char *filename, int size)
{
	FILE *fd;
	fd = fopen(filename, "w");
	for (int i = 0; i < size; i++) {
		printf("%lf\t%lf\n", independent[i], dependent[i]);
		fprintf(fd, "%lf %lf\n", independent[i], dependent[i]);
	}
	fclose(fd);
}
		
int read_data_from_file(double *independent, double *dependent, const char *filename)
{
	int n = 0;
	FILE *fd;

	fd = fopen(filename, "r");
	while (fscanf(fd, "%lf %lf", &independent[n], &dependent[n]) > 0)
		n++;

	fclose(fd);
	return n;
}

double min(double *A, int size)
{
	double min = A[0];
	for (int i = 1; i < size; i++)
		if (min > A[i])
			min = A[i];
	return min;
}

double max(double *A, int size)
{
	double max = A[0];
	for (int i = 1; i < size; i++)
		if (max < A[i])
			max = A[i];
	return max;
}

void show_help()
{
	printf("a - alpha value (default 0.1)\n");
	printf("g - data generation (by default)\n");
	printf("d - std for data generation (by default 0.1)\n");
	printf("n - amount of observation for data generation (by default 20)\n");
	printf("f - input filename\n");
}

//--------------------------------------------------------
// FUNCTION main
//--------------------------------------------------------
int main(int argc, char **argv)
{
	double b0, b1;
	double pval;
	int size;

	double linex[2] 	= {0};
	double liney[2] 	= {0};
	
	double alpha 		= 0.1;
	double std 		= 10;
	int n 			= 20;
	int need_generation 	= 1;
	
	char filename[256] 	= {0};
	double independent[256] = {0};
	double dependent[256]	= {0};

	char c;
	while ((c = getopt (argc, argv, "gn:d:f:a:")) != -1) {
		switch (c) {
			case 'a':
				sscanf(optarg, "%lf", &alpha);
				break;
			case 'n':
				sscanf(optarg, "%d", &n);
				break;
			case 'd':
				sscanf(optarg, "%lf", &std);
				break;
			case 'g':
				need_generation = 1;
				break;
			case 'f':
				need_generation = 0;
				sprintf(filename, "%s", optarg);
				break;
			case '?':
			default:
				show_help();
				return 1;
		}
	}

	if (need_generation)
		size = generate_data(independent, dependent, std, n);
	else
		size = read_data_from_file(independent, dependent, filename);

	if(size > 0) {
		linear_regression(independent, dependent, size, &b0, &b1);
		printf("y = %lfx + %lf\n\n", b1, b0);
		printdata(independent, dependent, DATASET_FILENAME, size);

		linex[0] = min(independent, size);
		linex[1] = max(independent, size);

		liney[0] = linex[0] * b1 + b0;
		liney[1] = linex[1] * b1 + b0;

		printdata(linex, liney, LINE_FILENAME, 2);
		pval = model_adequacy_check(independent, dependent, size, b0, b1);

		if (pval > 1.0 - alpha) 
			printf("H0 declined. Regression model is adequate\n");
		else 		
			printf("H0 accepted. Regression model is not adequate\n");

		system("gnuplot -p -e \"plot 'dataset.txt', 'line.txt' with l\"");
	}
	return EXIT_SUCCESS;
}


