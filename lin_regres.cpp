#include "lin_regres.h"
#include <boost/math/distributions/fisher_f.hpp>
#include <time.h>
#include <random>

using std::left; using std::fixed; using std::right; using std::scientific;
using std::setw;


//--------------------------------------------------------
// FUNCTION arithmetic_mean
//--------------------------------------------------------
static double arithmetic_mean(double* data, int size)
{
	double total = 0;
	for (int i = 0; i < size; i++)
		total += data[i];
	return total / size;
}

//--------------------------------------------------------
// FUNCTION mean_of_products
//--------------------------------------------------------
static double mean_of_products(double* data1, double* data2, int size)
{
	double total = 0;
	for (int i = 0; i < size; i++)
		total += (data1[i] * data2[i]);
	return total / size;
}

//--------------------------------------------------------
// FUNCTION variance
//--------------------------------------------------------
static double variance(double* data, int size)
{
	double squares[size];

	for (int i = 0; i < size; i++)
		squares[i] = data[i] * data[i];

	double mean_of_squares = arithmetic_mean(squares, size);
	double mean = arithmetic_mean(data, size);
	double square_of_mean = mean * mean;
	double variance = mean_of_squares - square_of_mean;

	return variance;
}

// --------------------------------------------------------
// FUNCTION linear_regression
// --------------------------------------------------------
void linear_regression(double* independent, double* dependent, int size, double *b0, double *b1)
{
	double independent_mean 	= arithmetic_mean(independent, size);
	double dependent_mean		= arithmetic_mean(dependent, size);
	double products_mean 		= mean_of_products(independent, dependent, size);
	double independent_variance	= variance(independent, size);

	*b1 = (products_mean - (independent_mean * dependent_mean) ) / independent_variance;
	*b0 = dependent_mean - (*b1 * independent_mean);
}


// --------------------------------------------------------
// FUNCTION linear_regression
// --------------------------------------------------------
double model_adequacy_check(double* independent, double* dependent, int size, double b0, double b1)
{
	double Dy	= variance(dependent, size);
	double DresY	= 0;

	for (int i  = 0; i < size; i++)
	{
		double f0 = b0 + b1*independent[i];
		DresY += 1.0/(double)size * (dependent[i] - f0) * (dependent[i] - f0);
	}

	double Ryx2 	= 1 - DresY/Dy;
	double Z 	=  Ryx2/((1 - Ryx2) / (size - 2));
	
	printf("Dy = %lf, DresY=%lf, Ryx = %lf Z = %lf\n", Dy, DresY, Ryx2, Z);

	using boost::math::fisher_f;
	fisher_f dist(1, size - 2);
	double p = cdf(dist, Z);
	printf("p-value=%lf\n", p);
	return p;
}

int generate_data(double* independent, double* dependent, double stddev, int n)
{
	int size = n;
	double mean = 0.0;

	srand(time(NULL));   // Initialization, should only be called once.
	std::mt19937 generator;
	std::normal_distribution<double> normal(mean, stddev);

	double b0 = ((double)(rand() % 10000 )) / 1000.0;  
	double b1 = ((double)(rand() % 10000 )) / 1000.0;  
	
	for (int i = 0; i < size; i++) {
		independent[i] = ((double) (rand() % 10000)) / 1000.0;
		dependent[i] = b0 + b1*independent[i] + normal(generator);
	}
	return size;
}
